#include "elf.h"

#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/stat.h>

#include <unistd.h>
#include <fcntl.h>

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

unsigned long
elf64_hash(const unsigned char *name)
{
	unsigned long h;
	h = 0;

	unsigned long g;
	while (*name) {
		h = (h << 4) + *name++;
		g = h & 0xF0000000;
		if (g != 0)
			h ^= g >> 24;
		h &= 0x0FFFFFFF;
	}

	return h;
}

uint32_t
elf64_ehdr_read(int file, elf64_ehdr *ehdr)
{
	ssize_t r_res;
	r_res = pread(file, ehdr, sizeof (elf64_ehdr), 0);
	if (r_res <= 0)
		return ELF64_EHDR_READ_ERR_PREAD;

	return ELF64_EHDR_READ_OK;
}

void
elf64_ehdr_print(elf64_ehdr *ehdr)
{
	printf("ident:   [" KBLU "%02X%02X%02X%02X %02X%02X%02X%02X"	
		" %02X%02X%02X%02X %02X%02X%02X%02X" KNRM "]\n",
		ehdr->e_ident[0x0], ehdr->e_ident[0x1],
		ehdr->e_ident[0x2], ehdr->e_ident[0x3],
		ehdr->e_ident[0x4], ehdr->e_ident[0x5],
		ehdr->e_ident[0x6], ehdr->e_ident[0x7],
		ehdr->e_ident[0x8], ehdr->e_ident[0x9],
		ehdr->e_ident[0xA], ehdr->e_ident[0xB],
		ehdr->e_ident[0xC], ehdr->e_ident[0xD],
		ehdr->e_ident[0xE], ehdr->e_ident[0xF]);
	printf("-> MAG0:    [" KBLU "0x%02X" KNRM "]\n", ehdr->e_ident[EI_MAG0]);
	printf("-> MAG1:    [" KBLU "%c" KNRM "]\n", ehdr->e_ident[EI_MAG1]);
	printf("-> MAG2:    [" KBLU "%c" KNRM "]\n", ehdr->e_ident[EI_MAG2]);
	printf("-> MAG3:    [" KBLU "%c" KNRM "]\n", ehdr->e_ident[EI_MAG3]);

	static const char *class_desc[] = {
		"it contains 32 bit objects",
		"it contains 64 bit objects"
	};
	elf64_byte class;
	class = ehdr->e_ident[EI_CLASS];
	printf("-> CLASS:   [" KBLU "%u" KNRM "], '" KGRN "%s" KNRM "'\n", class, class_desc[class - 1]);

	static const char *data_desc[] = {
		"object file data structures are little-endian",
		"object file data structures are big-endian",
	};
	elf64_byte data;
	data = ehdr->e_ident[EI_DATA];
	printf("-> DATA:    [" KBLU "%u" KNRM "], '" KGRN "%s" KNRM "'\n", data, data_desc[data - 1]);

	printf("-> VERSION: [" KBLU "%u" KNRM "]\n", ehdr->e_ident[EI_VERSION]);


	static const char *osabi_desc[] = {
		"System V ABI",
		"HP-UX OS",
		"Unkown"
	};
	elf64_byte osabi;
	osabi = ehdr->e_ident[EI_OSABI] > ELFOSABI_HPUX ? 2 : ehdr->e_ident[EI_OSABI];
	printf("-> OS ABI:  [" KBLU "%u" KNRM "], '" KGRN "%s" KNRM "'\n", ehdr->e_ident[EI_OSABI], osabi_desc[osabi]);	
	printf("-> ABI VER: [" KBLU "%u" KNRM "]\n", ehdr->e_ident[EI_ABIVERSION]);	

	printf("-> PADDING\n");

	static const char *e_type_desc[] = {
		"No file type",
		"Relocatable object file",
		"Executable file",
		"Shared object file",
		"Core file",
		"Enviornment-specific use",
		"Processor-specific use"
	};
	elf64_half e_type;
	e_type = ehdr->e_type;
	if (e_type >= ET_LOOS && e_type <= ET_HIOS)
		e_type = 5;
	else if (e_type >= ET_LOPROC)
		e_type = 6;
	printf("type:    [" KBLU "%u" KNRM "], '" KGRN "%s" KNRM "'\n", ehdr->e_type, e_type_desc[e_type]);

	static const char *e_machine_desc[] = {
		"None",
		"AT&T WE 32100",
		"SPARC",
		"Intel Architecture",
		"Motorola 68000",
		"Motorola 88000",
		"Intel 80486",
		"Intel 80860",
		"MIPS RS3000 Big-Endian",
		"MIPS RS4000 Big-Endian",
		"HPPA",
		"Sun's v8plus",
		"PowerPC",
		"PowerPC64",
		"Cell BE SPU",
		"SuperH",
		"SPARC v9 64-bit",
		"HP/Intel IA-64",
		"AMD x86-64",
		"IBM S/390",
		"Axis Communications 32-bit embedded processor",
		"NEC v850",
		"Renesas M32R",
		"Renesas H8/300,300H,H8S",
		"Panasonic/MEI MN10300, AM33",
		"ADI Blackfin Processor",
		"Fujitsu FR-V",
		"Atmel AVR32"
	};

	elf64_half e_machine;
	e_machine = ehdr->e_machine;
	switch (e_machine) {
	case 10:
		e_machine = 9;
		break;
	case 15:
		e_machine = 10;
		break;
	case 18:
		e_machine = 11;
		break;
	case 20:
		e_machine = 12;
		break;
	case 21:
		e_machine = 13;
		break;
	case 23:
		e_machine = 14;
		break;
	case 42:
		e_machine = 15;
		break;
	case 43:
		e_machine = 16;
		break;
	case 50:
		e_machine = 17;
		break;
	case 62:
		e_machine = 18;
		break;
	case 22:
		e_machine = 19;
		break;
	case 76:
		e_machine = 20;
		break;
	case 87:
		e_machine = 21;
		break;
	case 88:
		e_machine = 22;
		break;
	case 46:
		e_machine = 23;
		break;
	case 89:
		e_machine = 24;
		break;
	case 106:
		e_machine = 25;
		break;
	case 0x5441:
		e_machine = 26;
		break;
	case 0x18AD:
		e_machine = 27;
		break;
	}

	printf("machine: [" KBLU "%u" KNRM "], '" KGRN "%s" KNRM "'\n", ehdr->e_machine, e_machine_desc[e_machine]);
	printf("version: [" KBLU "%u" KNRM "]\n", ehdr->e_version);
	printf("entry:   [" KBLU "0x%016lX" KNRM "]\n", ehdr->e_entry);
	printf("flags:   [" KBLU "0x%08X" KNRM "]\n", ehdr->e_flags);
	printf("elf header size:      [" KBLU "%u" KNRM "]\n", ehdr->e_ehsize);
	printf("program header size:  [" KBLU "%u" KNRM "]\n", ehdr->e_phentsize);
	printf("section header size:  [" KBLU "%u" KNRM "]\n", ehdr->e_shentsize);
	printf("program header count: [" KBLU "%u" KNRM "]\n", ehdr->e_phnum);
	printf("section header count: [" KBLU "%u" KNRM "]\n", ehdr->e_shnum);
	printf("program headers start offset:   [" KBLU "%lu" KNRM "]\n", ehdr->e_phoff);
	printf("section headers start offset:   [" KBLU "%lu" KNRM "]\n", ehdr->e_shoff);
	printf("index of string section header: [" KBLU "%u" KNRM "]\n", ehdr->e_shstrndx);
}

uint32_t
elf64_shdrs_read(int file, elf64_ehdr *ehdr, elf64_shdr *shdrs)
{
	off_t file_off;
	file_off = ehdr->e_shoff;

	ssize_t bytes;
	for (elf64_half i = 0; i < ehdr->e_shnum; ++i) {
		bytes = pread(file, &shdrs[i], ehdr->e_shentsize, file_off);
		if (bytes != ehdr->e_shentsize)
			return ELF64_SHDRS_READ_ERR_PREAD;
		file_off += ehdr->e_shentsize;
	}
	
	return ELF64_SHDRS_READ_OK;
}

void
elf64_shdrs_print(int file, elf64_ehdr *ehdr, elf64_shdr *shdrs)
{
	uint8_t *strbuf;
	strbuf = malloc(shdrs[ehdr->e_shstrndx].sh_size);

	uint32_t res;
	res = elf64_sbuf_read(file, &shdrs[ehdr->e_shstrndx], strbuf);
	if (res != ELF64_SBUF_READ_OK) {
		free(strbuf);
		return;
	}

	static const char *type_desc[] = {
		"NULL",
		"PROGBITS",
		"SYMTAB",
		"STRTAB",
		"RELA",
		"HASH",
		"DYNAMIC",
		"NOTE",
		"NOBITS",
		"REL",
		"SHLIB",
		"DYNSYM",
		"UNKNOWN",
		"[OS]",
		"[PROC]"
	};

	printf("+------------------------------------------------------------- SECTION HEADERS -------------------------------------------------------------+\n");
	printf("| " KGRN "index" KNRM " |");
	printf("          " KGRN "name" KNRM "          |");
	printf("   " KGRN "type" KNRM "   |");
	printf(" " KGRN "flags" KNRM " |");
	printf("        " KGRN "addr" KNRM "        |");
	printf("  " KGRN "offset" KNRM "  |");
	printf("    " KGRN "size" KNRM "    |");
	printf("   " KGRN "link" KNRM "   |");
	printf("   " KGRN "info" KNRM "   |");
	printf(" " KGRN "align" KNRM " |");
	printf(" " KGRN "entry size" KNRM " |");
	printf("\n");
	printf("+-------+------------------------+----------+-------+--------------------+----------+------------+----------+----------+-------+------------+\n");
	for (elf64_half i = 0; i < ehdr->e_shnum; ++i) {
		printf("|  " KBLU "%03u" KNRM "  |", i);
		printf(" %22s |", (strbuf + shdrs[i].sh_name));

		elf64_word type;
		type = shdrs[i].sh_type;
		if (type >= SHT_HOOS && type <= SHT_LOOS)
			type = 13;
		else if (type >= SHT_HOPROC && type <= SHT_LOPROC)
			type = 14;
		else if (type > SHT_DYNSYM) 
			type = 12;
		printf(" " KBLU "%8s" KNRM " |", type_desc[type]);

		elf64_xword flag;
		flag = shdrs[i].sh_flags;
		char flag_w;
		flag_w = flag & SHF_WRITE ? 'W' : '-';
		char flag_a;
		flag_a = flag & SHF_ALLOC ? 'A' : '-';
		char flag_x;
		flag_x = flag & SHF_EXECINSTR ? 'X' : '-';
		char flag_o;
		flag_o = flag & SHF_MASKOS ? 'O' : '-';
		char flag_p;
		flag_p = flag & SHF_MASKPROC ? 'P' : '-';
		printf(" " KBLU "%c%c%c%c%c" KNRM " |", flag_w, flag_a, flag_x, flag_o, flag_p);

		printf(KBLU " 0x%016lX " KNRM "|", shdrs[i].sh_addr);
		printf(" " KBLU "%08lu" KNRM " |", shdrs[i].sh_offset);
		printf(" " KBLU "%010lu" KNRM " |", shdrs[i].sh_size);

		elf64_word link;
		link = shdrs[i].sh_link;
		if (link > SHT_DYNSYM)
			link = 12;
		printf(" " KBLU "%8s" KNRM " |", type_desc[link]);

		elf64_word info;
		info = shdrs[i].sh_info;
		if (info > SHT_DYNSYM)
			info = 12;
		printf(" " KBLU "%8s" KNRM " |", type_desc[info]);

		printf("  " KBLU "%03lu" KNRM "  |", shdrs[i].sh_addralign);
		printf(" " KBLU "%010lu" KNRM " |", shdrs[i].sh_entsize);

		printf("\n");
	}	

	free(strbuf);
}

uint32_t
elf64_sbuf_read(int file, elf64_shdr *shdr, uint8_t *buf)
{
	ssize_t bytes;
	bytes = pread(file, buf, shdr->sh_size, shdr->sh_offset);
	if (bytes != (ssize_t)shdr->sh_size)
		return ELF64_SBUF_READ_ERR_PREAD;

	return ELF64_SBUF_READ_OK;
}

void
elf64_syms_print(int file, elf64_ehdr *ehdr, elf64_shdr *shdrs)
{
	static const char *type_desc[] = {
		"NONE",
		"OBJECT",
		"FUNC",
		"SECTION",
		"FILE",
		"[OS]",
		"[PROC]",
		"UNKNOWN"
	};

	static const char *bind_desc[] = {
		"LOCAL",
		"GLOBAL",
		"WEAK",
		"[OS]",
		"[PROC]",
		"UNKNOWN"
	};

	for (elf64_half i = 0; i < ehdr->e_shnum; ++i) {
		if (shdrs[i].sh_type != SHT_DYNSYM && shdrs[i].sh_type != SHT_SYMTAB)
			continue;

		elf64_sym *symbuf;
		symbuf = malloc(shdrs[i].sh_size);

		uint32_t res;
		res = elf64_sbuf_read(file, &shdrs[i], (uint8_t*)symbuf);
		if (res != ELF64_SBUF_READ_OK)
			printf("ERR: failed to read the sym section\n");

		elf64_xword symcnt;
		symcnt = shdrs[i].sh_size / sizeof (elf64_sym);

		elf64_half stridx;
		stridx = shdrs[i].sh_link;

		uint8_t *strbuf;
		strbuf = malloc(shdrs[stridx].sh_size);

		res = elf64_sbuf_read(file, &shdrs[stridx], strbuf);
		if (res != ELF64_SBUF_READ_OK)
			printf("ERR: failed to read the link section\n");

		printf("+------------------------ [" KGRN "index" KNRM "{" KBLU "%05u" KNRM "}:" KGRN "count" KNRM "{" KBLU "%05lu" KNRM "}] ------------------------+\n", i, symcnt); 
		printf("| " KGRN "index" KNRM " |                     " KGRN "name" KNRM "                      |   " KGRN "type" KNRM "   |   " KGRN "bind" KNRM "   |\n");
		printf("+-----------------------------------------------------------------------------+\n");
		for (elf64_xword j = 0; j < symcnt; ++j) {
			printf("| " KBLU "%05lu" KNRM " |", j); 
			printf(" %45s |", strbuf + symbuf[j].st_name); 

			uint8_t type;
			type = symbuf[j].st_info & 0xF;
			if (type >= STT_LOOS && type <= STT_HIOS)
				type = 5;
			else if (type >= STT_LOPROC && type <= STT_HIPROC)
				type = 6;
			else if (type > STT_FILE)
				type = 7;
			printf(" " KBLU "%8s" KNRM " |", type_desc[type]); 

			uint8_t bind;
			bind = (uint8_t)symbuf[j].st_info >> 4;
			if (type >= STB_LOOS && type <= STB_HIOS)
				type = 3;
			else if (type >= STB_LOPROC && type <= STB_HIPROC)
				type = 4;
			else if (type > STB_WEAK)
				type = 5;
			printf(" " KBLU "%8s" KNRM " |", bind_desc[bind]); 
			printf("\n");
		}

		free(strbuf);
		free(symbuf);
	}
}

void
elf64_code_print(int file, elf64_ehdr *ehdr, elf64_shdr *shdrs)
{
	uint8_t *strbuf;
	strbuf = malloc(shdrs[ehdr->e_shstrndx].sh_size);

	uint32_t res;
	res = elf64_sbuf_read(file, &shdrs[ehdr->e_shstrndx], strbuf);
	if (res != ELF64_SBUF_READ_OK) {
		free(strbuf);
		return;
	}

	for (elf64_half i = 0; i < ehdr->e_shnum; ++i) {
		if ((shdrs[i].sh_flags & SHF_EXECINSTR) == 0)
			continue;

		printf("+-------------------- " KGRN "%s" KNRM " --------------------+", strbuf + shdrs[i].sh_name);

		uint8_t *codebuf;
		codebuf = malloc(shdrs[i].sh_size);

		uint32_t res;
		res = elf64_sbuf_read(file, &shdrs[i], codebuf);
		if (res != ELF64_SBUF_READ_OK)
			printf("ERR: failed to read the code section\n");

		for (elf64_xword j = 0; j < shdrs[i].sh_size; ++j) {
			if (j % 16 == 0)
				printf("\n");

			printf("%02X", codebuf[j]);

			if ((j + 1) % 8 == 0)
				printf(" ");
		}
		printf("\n");

		free(codebuf);
	}

	free(strbuf);
}
