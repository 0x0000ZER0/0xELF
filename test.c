#include "elf.h"

#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/stat.h>

#include <fcntl.h>
#include <unistd.h>

int
main(int argc, char *argv[])
{
	if (argc < 2) {
		printf("INF: usage \t test <bin>\n");
		return 0;
	}

	int file;
	file = open(argv[1], O_RDONLY);

	elf64_ehdr ehdr;

	uint32_t res;
	res = elf64_ehdr_read(file, &ehdr);
	if (res != ELF64_EHDR_READ_OK) {
		printf("ERR: could not read the elf header\n");
		close(file);
		return 1;
	}
	elf64_ehdr_print(&ehdr);

	elf64_shdr *shdrs;
	shdrs = malloc(ehdr.e_shentsize * ehdr.e_shnum);
	if (shdrs == NULL) {
		printf("ERR: could not allocate memory for section headers\n");
		close(file);
		return 1;
	}

	res = elf64_shdrs_read(file, &ehdr, shdrs);
	if (res != ELF64_SHDRS_READ_OK) {
		printf("ERR: could not read the section headers\n");
		free(shdrs);
		close(file);
		return 1;
	}
	elf64_shdrs_print(file, &ehdr, shdrs);
	elf64_syms_print(file, &ehdr, shdrs);
	elf64_code_print(file, &ehdr, shdrs);
		
	free(shdrs);
	close(file);
	return 0;
}

