#ifndef __ELF_H__
#define __ELF_H__

#include <stdint.h>

typedef uint32_t elf32_addr;
typedef uint16_t elf32_half;
typedef uint32_t elf32_off;
typedef int32_t  elf32_sword;
typedef uint32_t elf32_word;
typedef uint8_t  elf32_byte;

typedef uint64_t elf64_addr;
typedef uint64_t elf64_off;
typedef uint16_t elf64_half;
typedef uint32_t elf64_word;
typedef int32_t  elf64_sword;
typedef uint64_t elf64_xword;
typedef int64_t  elf64_sxword;
typedef uint8_t  elf64_byte;

enum {
	EI_MAG0 	= 0,  // file identification
	EI_MAG1 	= 1,  // file identification
	EI_MAG2 	= 2,  // file identification
	EI_MAG3 	= 3,  // file identification
	EI_CLASS 	= 4,  // file class
	EI_DATA 	= 5,  // data encoding
	EI_VERSION 	= 6,  // file version
	EI_OSABI 	= 7,  // OS/ABI identification
	EI_ABIVERSION 	= 8,  // ABI version
	EI_PAD 		= 9,  // start of padding
	EI_NIDENT 	= 16, // size of e_ident[]
};

enum {
	ELFCLASS32 = 1, // 32-bit objects
	ELFCLASS64 = 2  // 64-bit objects
};

enum {
	ELFDATA2LSB = 1, // object file data structures are little-endian
	ELFDATA2MSB = 2  // object file data structures are big-endian 
};

enum {
	ELFOSABI_SYSV 		= 0,  // System V ABI
	ELFOSABI_HPUX 		= 1,  // HP-UX operating System
	ELFOSABI_STANDALONE 	= 255 // standalone (embedded) application
};

enum {
	ET_NONE 	= 0, 	  // no file type
	ET_REL 		= 1, 	  // relocatable file
	ET_EXEC 	= 2, 	  // executable file
	ET_DYN 		= 3, 	  // shared object file
	ET_CORE 	= 4, 	  // core file
	ET_LOOS 	= 0xFE00, // environment-specific
	ET_HIOS 	= 0xFEFF, // environment-specific
	ET_LOPROC 	= 0xFF00, // processor-specific
	ET_HIPROC 	= 0xFFFF  // processor-specific
};

enum {
	EM_NONE 	= 0,  // no machine
	EM_M32 		= 1,  // AT&T WE 32100
	EM_SPARC 	= 2,  // SPARC
	EM_386 		= 3,  // Intel Architecture
	EM_68K 		= 4,  // Motorola 68000
	EM_88K 		= 5,  // Motorola 88000
	EM_860 		= 7,  // Intel 80860
	EM_MIPS 	= 8,  // MIPS RS3000 Big-Endian
	EM_MIPS_RS4_BE 	= 10, // MIPS RS4000 Big-Endian

	RESERVED_11 	= 11,
	RESERVED_12 	= 12,
	RESERVED_13 	= 13,
	RESERVED_14 	= 14,
	RESERVED_15 	= 15,
	RESERVED_16 	= 16
};

#define EM_NONE		0
#define EM_M32		1
#define EM_SPARC	2
#define EM_386		3
#define EM_68K		4
#define EM_88K		5
#define EM_486		6	
#define EM_860		7
#define EM_MIPS		8	/* MIPS R3000 (officially, big-endian only) */
#define EM_MIPS_RS3_LE	10	/* MIPS R3000 little-endian */
#define EM_MIPS_RS4_BE	10	/* MIPS R4000 big-endian */
#define EM_PARISC	15	/* HPPA */
#define EM_SPARC32PLUS	18	/* Sun's "v8plus" */
#define EM_PPC		20	/* PowerPC */
#define EM_PPC64	21	/* PowerPC64 */
#define EM_SPU		23	/* Cell BE SPU */
#define EM_SH		42	/* SuperH */
#define EM_SPARCV9	43	/* SPARC v9 64-bit */
#define EM_IA_64	50	/* HP/Intel IA-64 */
#define EM_X86_64	62	/* AMD x86-64 */
#define EM_S390		22	/* IBM S/390 */
#define EM_CRIS		76	/* Axis Communications 32-bit embedded processor */
#define EM_V850		87	/* NEC v850 */
#define EM_M32R		88	/* Renesas M32R */
#define EM_H8_300	46	/* Renesas H8/300,300H,H8S */
#define EM_MN10300	89	/* Panasonic/MEI MN10300, AM33 */
#define EM_BLACKFIN     106     /* ADI Blackfin Processor */
#define EM_FRV		0x5441	/* Fujitsu FR-V */
#define EM_AVR32	0x18AD	/* Atmel AVR32 */

enum {
	EV_NONE 	= 0, // invalid version
	EV_CURRENT 	= 1  // current version
};

enum {
	SHN_UNDEF 	= 0, 		// undefined section
	SHN_LOPROC 	= 0xFF00, 	// processor-specific use
	SHN_HIPROC 	= 0xFF1F, 	// processor-specific use
	SHN_LOOS 	= 0xFF20, 	// environment-specific use
	SHN_HIOS 	= 0xFF3F, 	// environment-specific use
	SHN_ABS 	= 0xFFF1, 	// indicates that the corresponding
					// reference is an absolute value
	SHN_COMMON 	= 0xFFF2 	// indicates a symbol that has been
					// declared as a common block
					// (Fortran COMMON or C tentative declaration)
};

typedef struct {
	elf32_byte e_ident[EI_NIDENT];
	elf32_half e_type;
	elf32_half e_machine;
	elf32_word e_version;
	elf32_addr e_entry;
	elf32_off  e_phoff;
	elf32_off  e_shoff;
	elf32_word e_flags;
	elf32_half e_ehsize;
	elf32_half e_phentsize;
	elf32_half e_phnum;
	elf32_half e_shentsize;
	elf32_half e_shnum;
	elf32_half e_shstrndx;
} elf32_ehdr;

typedef struct {
	elf64_byte e_ident[EI_NIDENT];
	elf64_half e_type;
	elf64_half e_machine;
	elf64_word e_version;
	elf64_addr e_entry;
	elf64_off  e_phoff;
	elf64_off  e_shoff;
	elf64_word e_flags;
	elf64_half e_ehsize;
	elf64_half e_phentsize;
	elf64_half e_phnum;
	elf64_half e_shentsize;
	elf64_half e_shnum;
	elf64_half e_shstrndx;
} elf64_ehdr;

enum {
	SHT_NULL 	= 0, 	// unused section header
	SHT_PROGBITS 	= 1, 	// contains information defined by the program
	SHT_SYMTAB 	= 2, 	// contains a linker symbol table
	SHT_STRTAB 	= 3, 	// contains a string table
	SHT_RELA 	= 4, 	// contains "Rela" type relocation entries
	SHT_HASH 	= 5, 	// contains a symbol hash table
	SHT_DYNAMIC 	= 6, 	// contains dynamic linking tables
	SHT_NOTE 	= 7, 	// contains note information
	SHT_NOBITS 	= 8, 	// contains uninitialized space, 
				// does not occupy any space in the file
	SHT_REL 	= 9, 	// contains "Rel" type relocation entries
	SHT_SHLIB 	= 10, 	// reserved 
	SHT_DYNSYM 	= 11, 	// contains a dynamic loader symbol table

	SHT_LOOS 	= 0x60000000, // environment-specific use
	SHT_HOOS 	= 0x6FFFFFFF, // environment-specific use
	SHT_LOPROC 	= 0x70000000, // processor-specific use
	SHT_HOPROC 	= 0x7FFFFFFF, // processor-specific use
};

enum {
	SHF_WRITE = 0x1, 	// section contains writable data
	SHF_ALLOC = 0x2, 	// section contains allocated memory
				// image of program
	SHF_EXECINSTR = 0x4, 	// section contains executable instructions
	
	SHF_MASKOS 	= 0x0F000000, // environment-specific use
	SHF_MASKPROC 	= 0xF0000000, // processor-specific use
};

enum {
	SHL_DYNAMIC 	= SHT_DYNAMIC,
	SHL_HASH 	= SHT_HASH,
	SHL_REL 	= SHT_REL,
	SHL_RELA 	= SHT_RELA,
	SHL_SYMTAB 	= SHT_SYMTAB,
	SHL_DYNSYM 	= SHT_DYNSYM
};

enum {
	SHI_REL 	= SHT_REL,
	SHI_RELA 	= SHT_RELA,
	SHI_SYMTAB 	= SHT_SYMTAB,
	SHI_DYNSYM 	= SHT_DYNSYM
};



typedef struct {
	elf64_word  sh_name;
	elf64_word  sh_type;
	elf64_xword sh_flags;
	elf64_addr  sh_addr;
	elf64_off   sh_offset;
	elf64_xword sh_size;
	elf64_word  sh_link;
	elf64_word  sh_info;
	elf64_xword sh_addralign;
	elf64_xword sh_entsize;
} elf64_shdr;

enum {
	STB_LOCAL 	= 0,  // not visible outside the object file
	STB_GLOBAL 	= 1,  // visible to all object files
	STB_WEAK 	= 2,  // global scope, lower precendence
	STB_LOOS 	= 10, // environment-specific use
	STB_HIOS 	= 12, // environment-specific use
	STB_LOPROC 	= 13, // processor-specific use
	STB_HIPROC 	= 15  // processor-specific use
};

enum {
	STT_NOTYPE 	= 0,  // no type specified
	STT_OBJECT 	= 1,  // data object
	STT_FUNC 	= 2,  // function entry point
	STT_SECTION 	= 3,  // symbol is associated with a section
	STT_FILE 	= 4,  // source file associated with the object file
	STT_LOOS 	= 10, // environment-specific use
	STT_HIOS 	= 12, // environment-specific use
	STT_LOPROC 	= 13, // processor-specific use
	STT_HIPROC 	= 15  // processor-specific use
};

typedef struct {
	elf64_word  st_name;
	elf64_byte  st_info;
	elf64_byte  st_other;
	elf64_half  st_shndx;
	elf64_addr  st_value;
	elf64_xword st_size;
} elf64_sym;

#define ELF64_R_SYM(x) ((x) >> 32)
#define ELF64_R_TYPE(x) ((x) & 0xFFFFFFFFL)	
#define ELF64_R_INFO(s, t) (((s) << 32) + ((t) & 0xFFFFFFFFL))

typedef struct {
	elf64_addr  r_offset;
	elf64_xword r_info;
} elf64_rel;

typedef struct {
	elf64_addr   r_offset;
	elf64_xword  r_info;
	elf64_sxword r_addend;
} elf64_rela;

enum {
	PT_NULL 	= 0, // unused entry
	PT_LOAD 	= 1, // loadable segment
	PT_DYNAMIC 	= 2, // dynamic linking tables
	PT_INTERP 	= 3, // program interpreter path name
	PT_NOTE 	= 4, // note sections
	PT_SHLIB 	= 5, // reserved
	PT_PHDR 	= 6, // program header tables

	PT_LOOS 	= 0x60000000, // environment-specific use
	PT_HOOS 	= 0x6FFFFFFF, // environment-specific use
	PT_LOPROC 	= 0x70000000, // processor-specific use
	PT_HOPROC 	= 0x7FFFFFFF  // processor-specific use
};

enum {
	PF_X = 0x1, // execute permission
	PF_W = 0x2, // write permission
	PF_R = 0x4, // read permission

	PF_MASKOS   = 0x00FF0000, // environment-specific use
	PF_MASKPROC = 0xFF000000  // processor-specific use
};

typedef struct {
	elf64_word  p_type;
	elf64_word  p_flags;
	elf64_off   p_offset;
	elf64_addr  p_vaddr;
	elf64_addr  p_paddr;
	elf64_xword p_filesz;
	elf64_xword p_memsz;
	elf64_xword p_align;
} elf64_phdr;

enum {
	DT_NULL 	= 0,  	// [ignored]
	DT_NEEDED 	= 1, 	// [d_val] -> the string table offset 
				// of the name of a needed library
	DT_PLTRELSZ 	= 2, 	// [d_val] -> total size, in bytes, 
				// of the relocation entries associated with
				// the procedure linkage table
	DT_PLTGOT 	= 3, 	// [d_ptr] -> contains an address associated
				// with the linkage table. The specific meaning
				// of this field is processor-dependent
	DT_HASH 	= 4, 	// [d_ptr] -> address of the symbol hash table
	DT_STRTAB 	= 5, 	// [d_ptr] -> address of the dynamic string table
	DT_SYMTAB 	= 6, 	// [d_ptr] -> address of the dynamic symbol table
	DT_RELA 	= 7, 	// [d_ptr] -> address of a relocation table 
				// with 'elf64_rela' entries
	DT_RELASZ 	= 8, 	// [d_val] -> total size, in bytes, 
				// of the 'DT_RELA' relocation table
	DT_RELAENT 	= 9, 	// [d_val] -> size, in bytes, 
				// of each 'DT_RELA' relocation entry
	DT_STRSZ 	= 10, 	// [d_val] -> total size, in bytes, 
				// of the string table
	DT_SYMENT 	= 11, 	// [d_val] -> size, in bytes, 
				// of each symbol table entry
	DT_INIT 	= 12, 	// [d_ptr] -> address of the initialization function
	DT_FINI 	= 13, 	// [d_ptr] -> address of the termination function
	DT_SONAME 	= 14,  	// [d_val] -> the string table offset 
				// of the name of this shared object
	DT_RPATH 	= 15, 	// [d_val] -> the string table offset 
				// of a shared library search path string
	DT_SYMBOLIC 	= 16, 	// [ignored] -> the presence of this 
				// dynamic table entry modifies the symbol resolution 
				// algorithm for references within the library. 
				// Symbols defined within the library are used to
				// resolve references before 
				// the dynamic linker searches the usual search path
	DT_REL 		= 17, 	// [d_ptr] -> address of a relocation 
				// table with 'elf64_rel' entries
	DT_RELSZ 	= 18, 	// [d_val] -> total size, in bytes, 
				// of the 'DT_REL' relocation table
	DT_RELENT 	= 19, 	// [d_val] -> size, in bytes, 
				// of each 'DT_REL' relocation entry
	DT_PLTREL 	= 20, 	// [d_val] -> type of relocation entry used 
				// for the procedure linkage table. 
				// The [d_val] member contains 
				// either 'DT_REL' or 'DT_RELA'
	DT_DEBUG 	= 21, 	// [d_ptr] -> reserved for debugger use
	DT_TEXTREL 	= 22, 	// [ignored] -> the presence of this dynamic table 
				// entry signals that the relocation table contains 
				// relocations for a non-writable segment
	DT_JMPREL 	= 23, 	// [d_ptr] -> address of the relocations 
				// associated with the procedure linkage table
	DT_BIND_NOW 	= 24, 	// [ignored] -> the presence of this dynamic 
				// table entry signals that the dynamic loader 
				// should process all relocations for this object
				// before transferring control to the program
	DT_INIT_ARRAY 	= 25, 	// [d_ptr] -> pointer to an array 
				// of pointers to initialization functions
	DT_FINI_ARRAY 	= 26, 	// [d_ptr] -> pointer to an array 
				// of pointers to termination functions
	DT_INIT_ARRAYSZ = 27, 	// [d_val] -> size, in bytes, 
				// of the array of initialization functions
	DT_FINI_ARRAYSZ = 28, 	// [d_val] -> size, in bytes, 
				// of the array of termination functions
	
	DT_LOOS 	= 0x60000000,  // environment-specific use
	DT_HOOS 	= 0x6FFFFFFF,  // environment-specific use
	DT_LOPROC 	= 0x70000000,  // processor-specific use
	DT_HOPROC 	= 0x7FFFFFFF,  // processor-specific use
};

typedef struct {
	elf64_sxword d_tag;
	union {
		elf64_xword d_val;
		elf64_addr  d_ptr;
	} d_un;
} elf64_dyn;

unsigned long
elf64_hash(const unsigned char*);

enum {
	ELF64_EHDR_READ_OK = 0,
	ELF64_EHDR_READ_ERR_PREAD
};

uint32_t
elf64_ehdr_read(int, elf64_ehdr*);

void
elf64_ehdr_print(elf64_ehdr*);

enum {
	ELF64_SHDRS_READ_OK = 0,
	ELF64_SHDRS_READ_ERR_PREAD
};

uint32_t
elf64_shdrs_read(int, elf64_ehdr*, elf64_shdr*);

void
elf64_shdrs_print(int, elf64_ehdr*, elf64_shdr*);

enum {
	ELF64_SBUF_READ_OK = 0,
	ELF64_SBUF_READ_ERR_PREAD
};

uint32_t
elf64_sbuf_read(int, elf64_shdr*, uint8_t*);

void
elf64_syms_print(int, elf64_ehdr*, elf64_shdr*);

void
elf64_code_print(int, elf64_ehdr*, elf64_shdr*);

#endif

